﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Levels : MonoBehaviour {


    
	public void LoadLevel(string levelname)
	{
		print("Loading level" + levelname);
		//loads the scene named levelname
		SceneManager.LoadScene(levelname);
	}

    public void LoadLevel1()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene(2);
    }

    public void LoadNextLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}
    public void QuitGame()
    {

        //UnityEditor.EditorApplication.isPlaying = false;

        Application.Quit();
    }


}
