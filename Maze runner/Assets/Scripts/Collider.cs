﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Collider : MonoBehaviour {

    public Text Score;

    int Scorenum = 0;

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Wall")
        {
            SceneManager.LoadScene(4);
        }

        if (col.gameObject.tag == "Star")
        {
            Scorenum++;
            DestroyObject(col.gameObject);
            Score.text = Scorenum.ToString();
            
        }

        if (col.gameObject.tag == "Next" && Scorenum == 3)
        {
            SceneManager.LoadScene(2);
        }

		if (col.gameObject.tag == "Final" && Scorenum == 5)
        {
            SceneManager.LoadScene(3);
        }

        if (col.gameObject.tag == "Trap")
        {
            SceneManager.LoadScene(4);
        }

    }

}
