﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour {

    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       



        // P Up
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Debug.Log("Player is pressing Up Arrow");
            GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 4f, 0f);
        }

        //P Down
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Debug.Log("Player is pressing Down Arrow");
            GetComponent<Rigidbody2D>().velocity = new Vector3(0f, -4f, 0f);

        }

        // P Left
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Debug.Log("Player is pressing Left Arrow");
            GetComponent<Rigidbody2D>().velocity = new Vector3(-4f, 0f, 0f);
        }

        //P Right
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Debug.Log("Player is pressing Right Arrow");
            GetComponent<Rigidbody2D>().velocity = new Vector3(4f, 0f, 0f);

        }

    }
}
